/*
                                      ____  _____ ____    _    _     ___
                                     |  _ \| ____|  _ \  / \  | |   / _ \
                                     | | | |  _| | | | |/ _ \ | |  | | | |
                                     | |_| | |___| |_| / ___ \| |__| |_| |
                                     |____/|_____|____/_/   \_\_____\___/

Desarrollado por: Luis Fernando Diaz Devos
Fecha: 28/05/2018
TechU-Col
*/

//Variables de trabajo
//Importamos
var express = require('express');
var bodyParser = require('body-parser');
var requestJson = require('request-json');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var app = express();
//variables de entorno de trabajo
var URLbase = "/colapi/v1/";
var port = process.env.PORT || 8080;
//Ruta de base de dato remota
var urlMlab = 'https://api.mlab.com/api/1/databases/colapidb_lfdd/collections/';
var apiKeyMLab = 'apiKey=5-_vDTuGpW4xbUeAbO_0v6bUOA39U2tI';
//Funcion para poder atender los Json del body
app.use(bodyParser.json());

//Indicador de instancia de inicio
app.listen(port, function(){
	console.log("");
	console.log("");
	console.log("   |     ###    ########  ####  ######   #######  ##       ");
	console.log("   |    ## ##   ##     ##  ##  ##    ## ##     ## ##       ");
	console.log("   |   ##   ##  ##     ##  ##  ##       ##     ## ##       ");
	console.log("   |  ##     ## ########   ##  ##       ##     ## ##       ");
	console.log("   |  ######### ##         ##  ##       ##     ## ##       ");
	console.log("   |  ##     ## ##         ##  ##    ## ##     ## ##       ");
	console.log("   |  ##     ## ##        ####  ######   #######  ######## ");
	console.log("   |");
	console.log("   |-> Manual: https://colapi-co.appspot.com/");
	console.log("   |->    API: https://colapi-co.appspot.com/colapi/v1");
	console.log("   |-> ApiCol: Trabajando en el puerto: " + port + "... ");
	console.log("");
});
//           __  __                         _
//          |  \/  | __ _ _ __  _   _  __ _| |
//          | |\/| |/ _` | '_ \| | | |/ _` | |
//          | |  | | (_| | | | | |_| | (_| | |
//          |_|  |_|\__,_|_| |_|\__,_|\__,_|_|
//
app.get('/',
 	function(req, res) {
	let fs = require('fs');
	fs.readFile('raml/index.html', 'utf-8', (err, data) => {
		if(err) {
			res.send({'Error: ':"err"});
		} else {
			res.send(data);
	  	}
	});
});//Funcion index
//           _   _                      _
//          | | | |___ _   _  __ _ _ __(_) ___  ___
//          | | | / __| | | |/ _` | '__| |/ _ \/ __|
//          | |_| \__ \ |_| | (_| | |  | | (_) \__ \
//           \___/|___/\__,_|\__,_|_|  |_|\___/|___/
//
app.get(URLbase + 'user',
 	function(req, res) {
		var httpClient = requestJson.createClient(urlMlab);
		var queryString = 'user?f={"_id": 0}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					res.status(200).send(body);
				}else{
					var respuesta = {"Msg":"Error en la carga de movement en mlab"}
					res.status(400).send(body);
				}
		});
});//Funcion encargada de consultar la lista de clientes
app.post(URLbase + 'user',
 	function(req, res) {
		//Variables de trabajo
		var usuExiste = "Nok"
		var avatar = req.body.avatar;
		var httpClient = requestJson.createClient(urlMlab);
		var queryString = 'user?f={"userEmail": 1,"_id": 0}&' + apiKeyMLab;
		//Validamos los datos enviados
		var estado = validarDatosUser(req);
		if (estado == "ok"){
			//Validamos si no tiene avatar
		 	if (!req.body.avatar){
		 		avatar = '';}
			//Buscamos la informacion del los usuarios a ver si existe
			httpClient.get(queryString,
				function(err,respuestaMLab,body){
					if (!err){
						//Validamos si el usuario existe
						for (var i = 0; i < respuestaMLab.length; i++) {
						    if (respuesta[i].userEmail == req.body.userEmail){
						    	usuExiste = "ok"}}
						if (usuExiste == 'Nok'){
							var httpClient2 = requestJson.createClient(urlMlab+'user?'
																+apiKeyMLab);
							var queryString = 'user?' + apiKeyMLab;
							//Componemos los datos del usuario
							var newUser = {
									    "userId" : respuestaMLab.length + 1,
								    "userName" : req.body.userName,
								"userApellido" : req.body.userApellido,
								   "userEmail" : req.body.userEmail,
								"userPassword" : req.body.userPassword,
								  "userStatus" : "Active",
								      "avatar" : avatar,
								     "cuentas" : req.body.cuentas};
						  //Creamos el usuario
							httpClient2.post('',newUser,
								function(err2,respuestaMLab2,body2){
									if (!err2){
										res.status(200).send(body2);
									}else{
										res.status(400).send({"Msg":"Error Mlab Usuario Cod: 03"});}
							});
						}else{
							res.status(400).send({"Msg":"Usuario ya existe"});}
					}else{
						res.status(400).send({"Msg":"Error Mlab Usuario Cod: 03"});}
			});
		}else{
			res.status(400).send(estado);}
});//Funcion encargada de crear un usuario
app.get(URLbase + 'user/:id',
 	function(req, res) {
		var id=req.params.id;
		var httpClient = requestJson.createClient(urlMlab);
		var queryString = 'user?f={"_id": 0}&q={"userId":'+id+'}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					res.status(200).send(body);
				}else{
					res.status(400).send({"Msg":"Error Mlab Usuario Cod: 04"});}
		});
});//Funcion encargada de ralizar la consulta de un cliente
app.put(URLbase + 'user/:id',
	function(req, res) {
		var id=req.params.id;
		var httpClient = requestJson.createClient(urlMlab);
		var queryString = 'user?q={"userId":'+id+'}&' + apiKeyMLab;
		var respuesta = "";
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
			 		if (req.body.userName){
				 		body[0].userName = req.body.userName ;}
			 		if (req.body.userApellido){
				 		body[0].userApellido = req.body.userApellido;}
			 		if (req.body.userEmail){
				 		body[0].userEmail = req.body.userEmail;}
			 		if (req.body.userPassword){
				 		body[0].userPassword = req.body.userPassword;}
				 	if (req.body.avatar){
				 		body[0].avatar = req.body.avatar;}
					if (req.body.cuentas){
					 	if (req.body.cuentas.length>0){
					 		//Se actualiza las cuentas del cliente
					 		body[0].cuentas = req.body.cuentas;}}
					//Se envia la informacion a guardar
					var rutaCons= 'user?f={"_id": 0}&q={"userId":'+id+'}&' + apiKeyMLab;
				 	var httpClient2 = requestJson.createClient(urlMlab + rutaCons);
					httpClient2.post('',body[0],
						function(err,respuestaMLab2,body2){
							respuesta = !err ? body : {"Msg":"Error Mlab Usuario Cod: 05"};
			 				res.send(respuesta);
					});
				}else{
					respuesta = {"Msg":"El usuario no existe"};
					res.send(respuesta);
				}
		});
});//Funcion encargada de ralizar la modificacion de un cliente
app.delete(URLbase + 'user/:id',
	function(req, res) {
		var id=req.params.id;
		var httpClient = requestJson.createClient(urlMlab);
		var queryString = 'user?f={"_id": 0}&q={"userId":'+id+'}&' + apiKeyMLab;
		var respuesta = ""
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					body[0].userStatus = "Disable";
					var ruta = 'user?f={"_id": 0}&q={"userId":'+id+'}&' + apiKeyMLab
				 	var httpClient2 = requestJson.createClient(urlMlab + ruta);
					httpClient2.put('',body[0],
						function(err2,respuestaMLab2,body2){
							if (!err2){
								res.status(204).send({"Msg":"Eliminacion logica ejecutada"});
							}else{
								res.status(400).send({"Msg":"Error Mlab Usuario Cod: 06"});
							}

					});
				}else{
					respuesta = {"Msg":"Error el usuario no existe"};
			 		res.send(respuesta);
				}
		});
});//Funcion encargada de ralizar la eliminacion de un cliente de forma logica
app.get(URLbase + 'user/:id/account',
	function(req, res) {
		var id=req.params.id;
		var httpClient = requestJson.createClient(urlMlab);
		var urlBase = 'account?f={"_id": 0}&q={"accountUserId":'+id+'}&';
		var queryString = urlBase + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					res.status(200).send(body);
				}else{
					res.status(400).send({"Msg":"Error Mlab Usuario Cod: 07"});
				}
		});
});//Funcion encargada de ralizar la consulta d las cuentas de un cliente
app.get(URLbase + 'user/:id/account/:iban',
	function(req, res) {
		var id=req.params.id;
		var iban=req.params.iban;
		var httpClient = requestJson.createClient(urlMlab);
		var urlBase = 'account?f={"_id": 0}&q={"accountUserId":'+id;
		var queryString = urlBase + ',"accountIban":"'+iban+'"}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					res.status(200).send(body);
				}else{
					res.status(400).send({"Msg":"Error Mlab Usuario Cod: 08"});
				}
		});
});//Funcion encargada de ralizar la consulta de una cuenta de un cliente
app.get(URLbase + 'user/:id/account/:iban/movement',
	function(req, res) {
		var iban=req.params.iban;
		var httpClient = requestJson.createClient(urlMlab);
		var urlBase =  'movement?f={"_id": 0}&q={"movementIban":"'+iban+'"}&'
		var queryString = urlBase + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					res.status(200).send(body);
				}else{
					res.status(400).send({"Msg":"Error Mlab Usuario Cod: 09"});
				}
		});
});//Funcion encargada de ralizar la consulta los movimiento de una cuenta para un cliente
//            ____                 _
//           / ___|   _  ___ _ __ | |_ __ _ ___
//          | |  | | | |/ _ \ '_ \| __/ _` / __|
//          | |__| |_| |  __/ | | | || (_| \__ \
//           \____\__,_|\___|_| |_|\__\__,_|___/
//
app.get(URLbase + 'account',
 	function(req, res) {
		var httpClient = requestJson.createClient(urlMlab);
		var queryString = 'account?f={"_id": 0}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					res.status(200).send(body);
				}else{
					res.status(400).send({"Msg":"Error Mlab Usuario Cod: 10"});
				}
		});
});//Funcion encargada de consultar la lista de cuentas
app.get(URLbase + 'account/:accountId',
 	function(req, res) {
		var accountId=req.params.accountId;
		var httpClient = requestJson.createClient(urlMlab);
		var urlBase = 'account?f={"_id": 0}&q={"accountIban":"'+accountId+'"}&';
		var queryString = urlBase + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					res.status(200).send(body);
				}else{
					res.status(400).send({"Msg":"Error Mlab Usuario Cod: 11"});
				}
		});
});//Funcion encargada de ralizar la consulta de una cuenta
app.delete(URLbase + 'account/:accountId',
	function(req, res) {
		var accountId=req.params.accountId;
		var httpClient = requestJson.createClient(urlMlab);
		var urlBase = 'account?f={"_id": 0}&q={"accountIban":"'+accountId+'"}&';
		var queryString =  + apiKeyMLab;
		var respuesta = ""
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					body[0].accountStatus = "Disable";
					var url2 ='account?f={"_id": 0}&q={"accountIban":"'+accountId+'"}&';
				 	var httpClient2 = requestJson.createClient(urlMlab+url2+apiKeyMLab);
					httpClient2.put('',body[0],
						function(err,respuestaMLab2,body2){
							if (!err){
								res.status(204).send(body);
							}else{
								res.status(400).send({"Msg":"Error Mlab Usuario Cod: 12"});
							}
					});
				}else{
					res.status(400).send({"Msg":"Error la cuenta no existe"});
				}
		});
});//Funcion encargada de ralizar la eliminacion logica de una cuenta
//           __  __            _           _            _
//          |  \/  | _____   _(_)_ __ ___ (_) ___ _ __ | |_ ___  ___
//          | |\/| |/ _ \ \ / / | '_ ` _ \| |/ _ \ '_ \| __/ _ \/ __|
//          | |  | | (_) \ V /| | | | | | | |  __/ | | | || (_) \__ \
//          |_|  |_|\___/ \_/ |_|_| |_| |_|_|\___|_| |_|\__\___/|___/
//
app.get(URLbase + 'movement',
 	function(req, res) {
		var httpClient = requestJson.createClient(urlMlab);
		var queryString = 'movement?f={"_id": 0}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					res.status(200).send(body);
				}else{
					res.status(400).send({"Msg":"Error Mlab Usuario Cod: 13"});
				}
		});
});//Funcion encargada de consultar la lista de movimientos
app.get(URLbase + 'movement/:movementId',
 	function(req, res) {
		var movementId=req.params.movementId;
		var httpClient = requestJson.createClient(urlMlab);
		var urlBase = 'movement?f={"_id": 0}&q={"movementId":'+movementId+'}&';
		var queryString = urlBase + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					res.status(200).send(body);
				}else{
					res.status(400).send({"Msg":"Error Mlab Usuario Cod: 14"});
				}
		});
});//Funcion encargada de ralizar la consulta de una cuenta
app.delete(URLbase + 'movement/:movementId',
	function(req, res) {
		var movementId=req.params.movementId;
		var httpClient = requestJson.createClient(urlMlab);
		var urlBase = 'movement?f={"_id": 0}&q={"movementId":'+movementId+'}&';
		var queryString = urlBase + apiKeyMLab;
		var respuesta = ""
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					body[0].movementStatus = "Disable";
				 	var httpClient2 = requestJson.createClient(urlMlab + queryString);
					httpClient2.put('',body[0],
						function(err,respuestaMLab2,body2){
							if (!err){
								res.status(200).send(body);
							}else{
								res.status(400).send({"Msg":"Error Mlab Usuario Cod: 15"});
							}
					});
				}else{
					res.status(400).send({"Msg":"Error el movimiento no existe"});
				}
		});
});//Funcion encargada de ralizar la eliminacion logica de una cuenta
//            ___   __ _      _
//           / _ \ / _(_) ___(_)_ __   __ _ ___
//          | | | | |_| |/ __| | '_ \ / _` / __|
//          | |_| |  _| | (__| | | | | (_| \__ \
//           \___/|_| |_|\___|_|_| |_|\__,_|___/
//
app.get(URLbase + 'office',
 	function(req, res) {
		var httpClient = requestJson.createClient(urlMlab);
		var queryString = 'office?f={"_id": 0}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					res.status(200).send(body);
				}else{
					res.status(400).send({"Msg":"Error Mlab Usuario Cod: 16"});
				}
		});
});//Funcion encargada de consultar la lista de movimientos
app.get(URLbase + 'office/:officeId',
 	function(req, res) {
		var officeId=req.params.officeId;
		var httpClient = requestJson.createClient(urlMlab);
		var urlBase = 'office?f={"_id": 0}&q={"officeId":'+officeId+'}&';
		var queryString = urlBase + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					res.status(200).send(body);
				}else{
					res.status(400).send({"Msg":"Error Mlab Usuario Cod: 17"});
				}
		});
});//Funcion encargada de ralizar la consulta de una cuenta
///----------------------------------------------------------------pendiente
app.delete(URLbase + 'office/:officeId',
	function(req, res) {
		var officeId=req.params.officeId;
		var httpClient = requestJson.createClient(urlMlab);
		var queryString = 'office?q={"officeId":'+officeId+'}&' + apiKeyMLab;
		var respuesta = "";
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					var dato = body[0]._id;
					var queryString2 = 'office/' + dato.$oid + '?' +apiKeyMLab;
				 	var httpClient2 = requestJson.createClient(urlMlab + queryString2);
					httpClient2.delete('',
						function(err,respuestaMLab2,body2){
							if (!err){
								res.status(200).send(body);
							}else{
								res.status(400).send({"Msg":"Error Mlab Usuario Cod: 18"});
							}
					});
				}else{
					res.status(400).send({"Msg":"Error la cuenta no existe"});
				}
		});
});//Funcion encargada de ralizar la eliminacion logica de una cuenta
//           ____                _
//          / ___|  ___  ___ ___(_) ___  _ __
//          \___ \ / _ \/ __/ __| |/ _ \| '_ \
//           ___) |  __/\__ \__ \ | (_) | | | |
//          |____/ \___||___/___/_|\___/|_| |_|
//
app.post(URLbase + 'session/login',
 	function(req, res) {
 		var respuesta ="";
		var userEmail = req.body.userEmail;
		var userPassword = req.body.userPassword;
		var httpClient = requestJson.createClient(urlMlab);
		var urlBase = 'user?f={"_id": 0}&q={"userEmail":"';
		var queryString = urlBase + userEmail+'"}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (body.length >0){
					if (body[0].userPassword==userPassword &&
						  body[0].userStatus=="Active"){
						var httpClient = requestJson.createClient(urlMlab);
						var urlBas2 = 'session?q={"sessionCorreo":"'+userEmail+'"}&';
						var queryString = urlBas2 + apiKeyMLab;
				 		var respuesta ="";
						httpClient.get(queryString,
							function(err,respuestaMLab,body2){
								if (body2.length >0){
									res.status(400).send({"Msg":"Error de credenciales"});
								}else{
									var urlB3 =urlMlab + 'session?';
							 		var httpClient2 = requestJson.createClient(urlB3+apiKeyMLab);
									var queryString = 'session?' + apiKeyMLab;
									var token = jwt.sign(req.body, 'colapiLfdd');
									var sessionNew = {
									   "sessionCorreo" : userEmail,
										  "sessionToken" : token,
										     "sessionId" : body[0].userId
									};
									httpClient2.post('',sessionNew,
										function(err,respuestaMLab,body3){
											if (!err){
												res.status(200).send(sessionNew);
											}else{
												res.status(400).send({"Msg":"Error Mlab Cod: 19"});
											}
									});}
						});
					}else{
						res.status(400).send({"Msg":"Error de credenciales"});
					}
				}else{
					res.status(400).send({"Msg":"Usuario con session Activa"});
				}
		});
});//Funcion encargada de dar acceso
app.post(URLbase + 'session/status',
 	function(req, res) {
 		var respuesta ="";
		var userEmail = req.body.userEmail;
		var userPassword = req.body.userPassword;
		var httpClient = requestJson.createClient(urlMlab);
		var urlBase = 'user?f={"_id": 0}&q={"userEmail":"'+userEmail+'"}&';
		var queryString = urlBase + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (body.length >0){
					if (body[0].userPassword == userPassword){
						var httpClient = requestJson.createClient(urlMlab);
						var urlBas2= 'session?f={"sessionToken": 1,"_id": 0}&q={"'
						var urlBas3= urlBase + '"sessionCorreo":"'+userEmail+'"}&'
						var queryString2 = urlBas3  + apiKeyMLab;
						httpClient.get(queryString2,
							function(err,respuestaMLab,body2){
								if (body2.length >0){
									res.status(200).send(body2[0]);
								}else{
									res.status(400).send({"Msg":"Error Mlab Cod: 20"});
								}
						});
					}else{
							res.status(400).send({"Msg":"Error de credenciales"});}
				}else{
						res.status(400).send({"Msg":"Error de credenciales"});}
		});
});//Funcion encargada validar el estado de la session
app.post(URLbase + 'session/logout',
 	function(req, res) {
		var sessionCorreo = req.body.sessionCorreo;
		var sessionToken = req.body.sessionToken;
		var httpClient = requestJson.createClient(urlMlab);
		var urlBase = 'session?q={"sessionCorreo":"'+sessionCorreo+'"}&';
		var queryString = urlBase + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (body.length >0){
					if (body[0].sessionToken == sessionToken){
						var dato = body[0]._id;
						var queryString2 = 'session/' + dato.$oid + '?' +apiKeyMLab;
				 		var httpClient2 = requestJson.createClient(urlMlab + queryString2);
						httpClient2.delete('',
						function(err2,respuestaMLab2,body2){
							if (!err2){
								res.status(200).send({"Msg":"Session Cerrada"});
							}else{
								res.status(400).send({"Msg":"Error Mlab Cod: 21"});
							}
						});
					}else{
							res.status(400).send({"Msg":"Error de credenciales"});}
				}else{
						res.status(400).send({"Msg":"Error de credenciales"});}
		});
});//Funcion encargada de cerrar una session

//           _____                 _
//          |  ___|   _ _ __   ___(_) ___  _ __   ___  ___
//          | |_ | | | | '_ \ / __| |/ _ \| '_ \ / _ \/ __|
//          |  _|| |_| | | | | (__| | (_) | | | |  __/\__ \
//          |_|   \__,_|_| |_|\___|_|\___/|_| |_|\___||___/
//
function ingresaMovimiento(monto,iban){
	var httpClient = requestJson.createClient(urlMlab);
	var queryString = 'movement?f={"_id": 0}&' + apiKeyMLab;
	httpClient.get(queryString,
		function(err,respuestaMLab,body){
			var urlBase = urlMlab + 'movement?';
			var httpClient2 = requestJson.createClient(urlBase + apiKeyMLab);
			var queryString = 'user?' + apiKeyMLab;
			var newMovement = {
				    "movementId" : respuestaMLab.length + 1,
				"movementStatus" : "Active",
				  "movementIban" : iban,
				 "movementMonto" : monto,
				 "movementFecha" : req.body.userEmail,
				  "movementTipo" : "tranferenciaIB"
			};
			httpClient2.post('',newMovement,
				function(err,respuestaMLab,body){
					mensaje = body;
					return("ok");
			});
	});
}//Funcion que registra un movimiento para una cuenta
function afectaCuentaSaldos(req,ibanOrigen,monto,accion){
	//Se traen los datos de la cuenta
	var accountId=req.params.accountId;
	var httpClient = requestJson.createClient(urlMlab);
	var urlBase = 'account?f={"_id": 0}&q={"accountIban":"'+ibanOrigen+'"}&';
	var queryString = urlBase + apiKeyMLab;

	httpClient.get(queryString,
		function(err,respuestaMLab,body){
			if (body.length >0){
				//Validamos que la operacion se pueda realizar
				if ((body[0].accountSaldo >= monto && accion == "-")||( accion == "+")){
					//verificamos el comportamiento del monto
					if (accion == "-"){
						body[0].accountSaldo = body[0].accountSaldo - monto;
					}else{
						body[0].accountSaldo = body[0].accountSaldo + monto;
					}
					//Ejecutamos la operacion
					var httpClient2 = requestJson.createClient(urlMlab + queryString);
					httpClient2.put('',body[0],
						function(err2,respuestaMLab2,body2){
							if (!err2){
								return (true);
							}else{
								return({"Msg":"Error con la cuenta al descargar el dinero"});}
					});
				}else{
			 		return({"Msg":"Saldo insuficiente"});}
			}else{
		 		return({"Msg":"Cuenta no existe"});}
	});

}//Funcion que actualiza el saldo de una cuenta
function validarDatosUser(req) {
	var mensaje = {"Msg":"Error en MLab"}
	if (req.body.userName ){
		if (req.body.userApellido){
			if (req.body.userEmail){
				if (req.body.userPassword){
					if (req.body.cuentas){
						if (req.body.cuentas.length>0){
							mensaje = 'ok';
						}else{mensaje = ({"Msg":"Cuenta no informada"});}
					}else{mensaje = ({"Msg":"Cuentas no informadas"});}
				}else{mensaje = ({"Msg":"Password no informado"});}
			}else{mensaje = ({"Msg":"Email no informado"});}
		}else{mensaje = ({"Msg":"Apellido no informado"});}
	}else{mensaje = ({"Msg":"Usuario no informado"});}
	return mensaje;
}
//Fin del codigo                                                               ™®LFDD
